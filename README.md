A RESTful API or service framework built on top of light-4j

[Developer Chat](https://gitter.im/networknt/light-rest-4j) |
[Documentation](https://doc.networknt.com/style/light-rest-4j/) |
[Contribution Guide](CONTRIBUTING.md) |

[![Build Status](https://travis-ci.org/networknt/light-rest-4j.svg?branch=master)](https://travis-ci.org/networknt/light-rest-4j)


## Light-rest-4j framework build rest-API based microservices on the light-4j platform

 现有微服务开发平台[评级结果](https://www.gajotres.net/best-available-java-restful-micro-frameworks/)； Light-4j 的restful 框架（light-rest-4j）评级5星。


## Summary

Light-rest-4j is a framework that is designed to speed up RESTful API development and deployment.
On top of light-4j, it has several middleware handlers specifically designed around Swagger 2.0 and OpenAPI 3.0 specifications.
With the specification ready, you can scaffold project with light-codegen and the specification will be loaded during the runtime to to enable JWT scope verification and schema validation for the request.




## Framework usage workflow diagram



![workflow](docs/light-4j.png)


### 开发基于REST-API 的微服务，参考[教程](https://www.networknt.com/tutorial/rest/)